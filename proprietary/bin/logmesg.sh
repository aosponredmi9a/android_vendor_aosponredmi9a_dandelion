#!/system/bin/sh
# logmesg.sh
cd /cache/
dmesg > dmesg.txt
getprop > getprop.txt
ps -efZ > ps.txt
ls -all /data/adb/ > lsdata.txt
ls -all /system/bin/ > lssystem.txt
logcat > logcat.txt
