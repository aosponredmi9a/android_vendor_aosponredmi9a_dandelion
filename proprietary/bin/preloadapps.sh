#!/system/bin/sh
# preloadapps.sh
if [ ! -f "/data/app/preloadapps.lock" ]; then
echo "preloadapps ..." 
unzip /product/zipres/preloadapps.zip -d /data/app/
echo "preloadapps completed !" > /data/app/preloadapps.lock
chmod 0770 /data/app/preloadapps.lock
sync
fi
